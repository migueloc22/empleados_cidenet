<div align="center" id="top"> 
  <img src="./app.png" alt="Empleados_cidenet" />

  &#xa0;

  <!-- <a href="https://empleados_cidenet.netlify.app">Demo</a> -->
</div>

<h1 align="center">Empleados_cidenet</h1>

<p align="center">
  <!-- <img alt="gitlab top language" src="https://img.shields.io/gitlab/languages/top/{{migueloc22}}/empleados_cidenet?color=56BEB8"> -->

  <!-- <img alt="gitlab language count" src="https://img.shields.io/gitlab/languages/count/{{migueloc22}}/empleados_cidenet?color=56BEB8"> -->

  <!-- <img alt="Repository size" src="https://img.shields.io/gitlab/repo-size/{{migueloc22}}/empleados_cidenet?color=56BEB8"> -->

  <!-- <img alt="License" src="https://img.shields.io/gitlab/license/{{migueloc22}}/empleados_cidenet?color=56BEB8"> -->

  <!-- <img alt="gitlab issues" src="https://img.shields.io/gitlab/issues/{{YOUR_gitlab_USERNAME}}/empleados_cidenet?color=56BEB8" /> -->

  <!-- <img alt="gitlab forks" src="https://img.shields.io/gitlab/forks/{{YOUR_gitlab_USERNAME}}/empleados_cidenet?color=56BEB8" /> -->

  <!-- <img alt="gitlab stars" src="https://img.shields.io/gitlab/stars/{{YOUR_gitlab_USERNAME}}/empleados_cidenet?color=56BEB8" /> -->
</p>

<!-- Status -->

<!-- <h4 align="center"> 
	🚧  Empleados_cidenet 🚀 Under construction...  🚧
</h4> 

<hr> -->

<p align="center">
  <a href="#dart-about">Sobre</a> &#xa0; | &#xa0; 
  <a href="#sparkles-features">Características</a> &#xa0; | &#xa0;
  <a href="#rocket-technologies">Tecnologías</a> &#xa0; | &#xa0;
  <a href="#white_check_mark-requirements">Requisitos</a> &#xa0; | &#xa0;
  <a href="#checkered_flag-starting">Starting</a> &#xa0; | &#xa0;
  <!-- <a href="#memo-license">License</a> &#xa0; | &#xa0; -->
  <a href="https://gitlab.com/{{migueloc22}}" target="_blank">Author</a>
</p>

<br>

## :dart: Sobre ##

Dentro de los procesos de Talento Humano, Cidenet S.A.S. requiere de un sistema que le permita registrar el ingreso y la salida de sus empleados, así como administrar su información. Actualmente el proceso se realiza de manera manual sobre una hoja de cálculo, lo cual funciona pero impide alimentar otros procesos para los cuales es importante esta información, así como llevar de manera óptima el registro y administración de la misma.

## :sparkles: Características ##

:heavy_check_mark: 
      • Registro de empleados: El usuario podrá registrar un empleado nuevo, para ello deberá registrar la siguiente información:

        ◦ Primer Apellido: Solo permite caracteres de la A a la Z, mayúscula, sin acentos ni Ñ. Es requerido y su longitud máxima será de 20 letras.
        ◦ Segundo Apellido: Solo permite caracteres de la A a la Z, mayúscula, sin acentos ni Ñ. Es requerido y su longitud máxima será de 20 letras.
        ◦ Primer Nombre: Solo permite caracteres de la A a la Z, mayúscula, sin acentos ni Ñ Es requerido y su longitud máxima será de 20 letras.
        ◦ Otros Nombres: Solo permite caracteres de la A a la Z, mayúscula, sin acentos ni Ñ, y el caracter espacio entre nombres. Es opcional y su longitud máxima será de 50 letras.
        ◦ País del empleo: País para el cual el empleado prestará sus servicios, podrá ser Colombia o Estados Unidos.
        ◦ Tipo de Identificación: Será el tipo de identificación, por ejemplo, Cédula de Ciudadanía, Cédula de Extranjería, Pasaporte, Permiso Especial. Debe ser una lista desplegable en la que el usuario seleccione el tipo de identificación correspondiente.
        ◦ Número de Identificación: Es el número de identificación del empleado, debe ser alfanumérico permitiendo los siguientes conjuntos de caracteres (a-z / A-Z / 0-9 / -). No podrán existir dos empleados con el mismo número y tipo de identificación. Su longitud máxima serán 20 caracteres.
        ◦ Correo electrónico: Se debe generar automáticamente, sin intervención del usuario, con el siguiente formato

        <PRIMER_NOMBRE>.<PRIMER_APELLIDO>.<ID>@<DOMINIO>

        El DOMINIO será cidenet.com.co para Colombia y cidenet.com.us para Estados Unidos.
        No podrán existir 2 empleados con el mismo correo electrónico, en caso de que ya exista se debe agregar un valor numérico adicional secuencial (ID).
        El correo electrónico debe tener un formato válido desde su generación automática, no puede ser intervenido por el usuario para ajustar su formato y su longitud máxima será de 300 caracteres.	
        ◦ Fecha de ingreso: No podrá ser superior a la fecha actual, pero sí podrá ser hasta un mes menor, ya que es posible que el usuario no haya podido registrarlo el día en que ingresó.
        ◦ Área: Será el área para la cual fue contratado el empleado, puede ser, Administración, Financiera, Compras, Infraestructura, Operación, Talento Humano, Servicios Varios, etc. Debe ser una lista desplegable en la que el usuario seleccione el área correspondiente.
        ◦ Estado: Siempre será Activo, no podrá ser modificado por el usuario.
        ◦ Fecha y hora de registro: Mostrará la fecha y hora del registro en formato DD/MM/YYYY HH:mm:ss, no puede ser editado.
;\
:heavy_check_mark: Consulta de empleados: Esta pantalla será la pantalla de inicio del sistema. Debe ser una pantalla en la cual se listen todos los empleados de Cidenet S.A.S., hasta 10 por página. Se podrán filtrar los empleados por Primer Nombre, Otros Nombres, Primer Apellido, Segundo Apellido, Tipo de Identificación, Número de Identificación, País del empleo, Correo electrónico y/o Estado. Cada registro de empleado tendrá la opción de ser editado mediante la funcionalidad de edición de empleado, o de ser eliminado.;\
:heavy_check_mark: Edición de empleados: Una vez seleccionada la opción de editar el empleado en la funcionalidad de consulta de empleados se abrirá la funcionalidad de edición de empleados, la cual tendrá los mismos campos de la funcionalidad de registro de empleados, permitiendo modificarlos todos (excepto la fecha de registro), incluso su tipo y número de identificación, en caso de que se modifiquen los nombres y/o apellidos, el sistema re-generará su dirección de correo electrónico. A diferencia de la funcionalidad de registro de empleados, no se tendrá fecha de registro sino de edición.;

## :rocket: Tecnologías ##

En este proyecto se utilizaron las siguientes herramientas:

- [Git](https://git-scm.com),
- [Mysql](https://www.mysql.com/) 
- [Python](https://www.python.org/)
- [Pip](https://www.python.org/)
- [FastApi](https://fastapi.tiangolo.com/es/)

## :white_check_mark: Requisitos ##

Antes De comenzar :checkered_flag:, debe tener [Git](https://git-scm.com), [Mysql](https://www.mysql.com/),Restarar el Backup de archivo Backup.sql y [Python](https://www.python.org/) instalado.

## :checkered_flag: Starting ##

```bash
# Clonar el proyecto
$ git clone https://gitlab.com/migueloc22/empleados_cidenet.git

# Entrar
$ cd empleados_cidenet
#restaurar la base de datos
mysql -u [user] -p [database_name] < backup.sql
# Instalar entonor trabajo
  pip3 install virtualenv
#levantar entorno linux
  source venv/bin/activate
#Entrar app
cd app
# Instalar dependencies
$ pip install -r  requirements.txt
# para configurar la base de datos debe hacerlo en el archivo app/config/db.py
# Arancar el proyecto
$ uvicorn main:app 
# pruebas unitarias
$ pytest

# El servidor se inicia <http://localhost:8000>
```

<!-- ## :memo: License ##
Este proyecto está bajo licencia del MIT. Para obtener más detalles, consulte el [LICENSE](LICENSE.md) file.


Made with :heart: by <a href="https://gitlab.com/{{migueloc22}}" target="_blank">{{Miguel Cifuentes}}</a>

&#xa0;

<a href="#top">Back to top</a> -->
