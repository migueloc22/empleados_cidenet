-- MariaDB dump 10.19  Distrib 10.9.4-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: cidenet_employee
-- ------------------------------------------------------
-- Server version	10.9.4-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `area`
--

DROP TABLE IF EXISTS `area`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `area` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `area` varchar(70) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `area`
--

LOCK TABLES `area` WRITE;
/*!40000 ALTER TABLE `area` DISABLE KEYS */;
INSERT INTO `area` VALUES
(1,'ADMINISTRACIÓN'),
(2,'FINANCIERA'),
(3,'COMPRAS'),
(4,'INFRAESTRUCTURA'),
(5,'OPERACIÓN'),
(6,'TALENTO HUMANO'),
(7,'SERVICIOS VARIOS');
/*!40000 ALTER TABLE `area` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `document_type`
--

DROP TABLE IF EXISTS `document_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `document_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `document_type`
--

LOCK TABLES `document_type` WRITE;
/*!40000 ALTER TABLE `document_type` DISABLE KEYS */;
INSERT INTO `document_type` VALUES
(1,'CEDULA DE CIUDADANIA','CC'),
(2,'CÉDULA DE EXTRANJERÍA','CE'),
(3,'PASAPORTE','PP'),
(4,'PERMISO ESPECIAL','PE');
/*!40000 ALTER TABLE `document_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employees`
--

DROP TABLE IF EXISTS `employees`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `employees` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name1` varchar(20) DEFAULT NULL,
  `name2` varchar(50) DEFAULT NULL,
  `ape1` varchar(20) DEFAULT NULL,
  `ape2` varchar(20) DEFAULT NULL,
  `email` varchar(300) DEFAULT NULL,
  `number_document` varchar(12) DEFAULT NULL,
  `status` varchar(20) DEFAULT 'ACTIVO',
  `date_time_register` datetime DEFAULT current_timestamp(),
  `date_entry` date DEFAULT NULL,
  `fk_id_employment_country` int(11) DEFAULT NULL,
  `fk_id_area` int(11) DEFAULT NULL,
  `fk_id_document_type` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  KEY `fk_id_employment_country` (`fk_id_employment_country`),
  KEY `fk_id_area` (`fk_id_area`),
  KEY `fk_id_document_type` (`fk_id_document_type`),
  CONSTRAINT `employees_ibfk_1` FOREIGN KEY (`fk_id_employment_country`) REFERENCES `employment_country` (`id`),
  CONSTRAINT `employees_ibfk_2` FOREIGN KEY (`fk_id_area`) REFERENCES `area` (`id`),
  CONSTRAINT `employees_ibfk_3` FOREIGN KEY (`fk_id_document_type`) REFERENCES `document_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employees`
--

LOCK TABLES `employees` WRITE;
/*!40000 ALTER TABLE `employees` DISABLE KEYS */;
INSERT INTO `employees` VALUES
(1,'JUAN','GUILLERMO','DE LA CALLE MORA','RR','juan.delacallemora@cidenet.com.co','13255f','ACTIVO','2022-12-29 09:04:43','2022-12-29',1,1,1),
(3,'JUAN','GUILLERMO','DE LA CALLE MORA','RR','juan.delacallemora.13255f@cidenet.com.co','13255f','ACTIVO','2022-12-29 09:59:03','2022-12-29',1,1,1),
(4,'JUAN','GUILLERMO','DE LA CALLE MORAL','ROJAS','juan.delacallemoral@cidenet.com.co','13255f','ACTIVO','2022-12-29 09:59:27','2022-12-29',1,1,1),
(5,'CAMILO','GUILLERMO','DE LA CALLE MORAL','ROJA','camilo.delacallemoral.13255f@cidenet.com.co','13255f','ACTIVO','2022-12-29 10:27:27','2022-12-29',1,1,1),
(6,'JUAN','GUILLERMO','DE LA CALLE MORAL','RR','juan.delacallemoral.13255f3@cidenet.com.co','13255f3','ACTIVO','2022-12-29 16:21:26','2022-12-28',1,1,1),
(7,'CAMILA','ANGEL','DE LA CALLE MORAL','RR','camila.delacallemoral@cidenet.com.co','13255f35','ACTIVO','2022-12-29 16:21:31','2022-12-28',1,1,1),
(8,'JUAN','GUILLERMO','DE LA CALLE MORAL','RR','juan.delacallemoral.13255f357@cidenet.com.co','13255f357','ACTIVO','2022-12-29 16:21:35','2022-12-28',1,1,1),
(9,'JUAN','GUILLERMO','DE LA CALLE MORAL','RR','juan.delacallemoral.13255f3578@cidenet.com.co','13255f3578','ACTIVO','2022-12-29 16:21:38','2022-12-28',1,1,1),
(10,'JUAN','GUILLERMO','DE LA CALLE MORAL','RR','juan.delacallemoral.13255f3578r@cidenet.com.co','13255f3578r','ACTIVO','2022-12-29 16:21:42','2022-12-28',1,1,1),
(11,'JUAN','GUILLERMO','DE LA CALLE MORAL','RR','juan.delacallemoral.13455f3578r@cidenet.com.co','13455f3578r','ACTIVO','2022-12-29 16:21:46','2022-12-28',1,1,1),
(12,'JUAN','GUILLERMO','DE LA CALLE MORAL','RR','juan.delacallemoral.13445f3578r@cidenet.com.co','13445f3578r','ACTIVO','2022-12-29 16:21:51','2022-12-28',1,1,1),
(13,'JUAN','GUILLERMO','DE LA CALLE MORAL','RR','juan.delacallemoral.134455578r@cidenet.com.co','134455578r','ACTIVO','2022-12-29 16:21:57','2022-12-28',1,1,1),
(14,'JUAN','GUILLERMO','DE LA CALLE MORAL','RR','juan.delacallemoral.137455578r@cidenet.com.co','137455578r','ACTIVO','2022-12-29 16:22:01','2022-12-28',1,1,1),
(15,'JUAN','GUILLERMO','DE LA CALLE MORAL','RR','juan.delacallemoral.137458578r@cidenet.com.co','137458578r','ACTIVO','2022-12-29 16:22:06','2022-12-28',1,1,1),
(16,'JUAN','GUILLERMO','DE LA CALLE MORAL','RR','juan.delacallemoral.138578r@cidenet.com.co','138578r','ACTIVO','2022-12-29 16:22:13','2022-12-28',1,1,1),
(17,'JUAN','GUILLERMO','DE LA CALLE MORAL','RR','juan.delacallemoral.138578qr@cidenet.com.co','138578qr','ACTIVO','2022-12-29 16:22:18','2022-12-28',1,1,1),
(18,'JUAN','GUILLERMO','DE LA CALLE MORAL','RR','juan.delacallemoral.138579qr@cidenet.com.co','138579qr','ACTIVO','2022-12-29 16:22:23','2022-12-28',1,1,1),
(19,'JUAN','GUILLERMO','DE LA CALLE MORAL','RR','juan.delacallemoral.138579qr1@cidenet.com.co','138579qr1','ACTIVO','2022-12-29 16:22:47','2022-12-28',1,1,1),
(20,'JUAN','GUILLERMO','DE LA CALLE MORAL','RR','juan.delacallemoral.138579qr2@cidenet.com.co','138579qr2','ACTIVO','2022-12-29 16:22:51','2022-12-28',1,1,1),
(21,'JUAN','GUILLERMO','DE LA CALLE MORAL','RR','juan.delacallemoral.138579qr3@cidenet.com.co','138579qr3','ACTIVO','2022-12-29 16:22:55','2022-12-28',1,1,1),
(22,'FABIAN','ANGEL','DE LA CALLE MORAL','RR','fabian.delacallemoral@cidenet.com.co','13255f35','ACTIVO','2022-12-29 16:22:59','2022-12-28',1,1,1),
(23,'JUAN','GUILLERMO','DE LA CALLE MORAL','RR','juan.delacallemoral.138579qr5@cidenet.com.co','138579qr5','ACTIVO','2022-12-29 16:23:04','2022-12-28',1,1,1),
(24,'JUAN','GUILLERMO','DE LA CALLE MORAL','RR','juan.delacallemoral.138579qr7@cidenet.com.co','138579qr7','ACTIVO','2022-12-29 16:23:09','2022-12-28',1,1,1),
(25,'JUAN','GUILLERMO','DE LA CALLE MORAL','RR','juan.delacallemoral.138579qr8@cidenet.com.co','138579qr8','ACTIVO','2022-12-29 16:23:12','2022-12-28',1,1,1),
(26,'JUAN','GUILLERMO','DE LA CALLE MORAL','RR','juan.delacallemoral.138579qr9@cidenet.com.co','138579qr9','ACTIVO','2022-12-29 16:23:20','2022-12-28',1,1,1),
(27,'JUAN','GUILLERMO','DE LA CALLE MORAL','RR','juan.delacallemoral.138579qr12@cidenet.com.co','138579qr12','ACTIVO','2022-12-29 16:23:27','2022-12-28',1,1,1),
(28,'JUAN','GUILLERMO','DE LA CALLE MORAL','RR','juan.delacallemoral.1385129qr12@cidenet.com.co','1385129qr12','ACTIVO','2022-12-29 16:24:10','2022-12-28',1,1,1),
(29,'JUAN','GUILLERMO','DE LA CALLE MORAL','RR','juan.delacallemoral.1385129qr15@cidenet.com.co','1385129qr15','ACTIVO','2022-12-29 16:24:15','2022-12-28',1,1,1),
(30,'JUAN','GUILLERMO','DE LA CALLE MORAL','RR','juan.delacallemoral.1385129qr17@cidenet.com.co','1385129qr17','ACTIVO','2022-12-29 16:24:19','2022-12-28',1,1,1);
/*!40000 ALTER TABLE `employees` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employment_country`
--

DROP TABLE IF EXISTS `employment_country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `employment_country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country` varchar(70) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employment_country`
--

LOCK TABLES `employment_country` WRITE;
/*!40000 ALTER TABLE `employment_country` DISABLE KEYS */;
INSERT INTO `employment_country` VALUES
(1,'COLOMBIA'),
(2,'ESTADOS UNIDOS');
/*!40000 ALTER TABLE `employment_country` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-12-30 15:16:02
