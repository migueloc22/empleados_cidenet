from fastapi import FastAPI
from fastapi.testclient import TestClient
from main import app
client = TestClient(app)

def test_():
    """
    Test unitario para el tema de paginacion 
    """
    response = client.get("employee/2")
    assert response.status_code == 200
    #assert response.json() == {"msg": "Hello World"}
# test de name 1
    """
        requerimento : 
        Primer Apellido: Solo permite caracteres de la A a la Z,
        mayúscula, sin acentos ni Ñ. 
        Es requerido y su longitud máxima será de 20 letras.
    """
def test_validacion_acentos_name1():
    """
        validar si tiene acentos en primer nombres
    """
    response = client.post(
        "/employee",
        json={
                "name1": "JUAÑ",
                "name2": "GUILLERMO",
                "ape1": "DE LA CALLE MORAL",
                "ape2": "RR",
                "email": "string",
                "number_document": "1385129qr17",
                "date_entry": "2022-12-28",
                "fk_id_employment_country": 1,
                "fk_id_area": 1,
                "fk_id_document_type": 1
            },
    )
    # print(response.json())
    assert response.status_code == 422
    assert response.json() == {
        "detail": [
            {
                "loc": [
                    "body",
                    "name1"
                ],
                "msg": "No permite nombre caracteres especiales",
                "type": "value_error"
            }
        ]
    }
def test_validacion_minusculas_name1():
    """
        validar si tiene minusculas en primer nombres
    """
    response = client.post(
        "/employee",
        json={
                "name1": "JUAn",
                "name2": "GUILLERMO",
                "ape1": "DE LA CALLE MORAL",
                "ape2": "RR",
                "email": "string",
                "number_document": "1385129qr17",
                "date_entry": "2022-12-28",
                "fk_id_employment_country": 1,
                "fk_id_area": 1,
                "fk_id_document_type": 1
            },
    )
    # print(response.json())
    assert response.status_code == 422
    assert response.json() == {
        "detail": [
            {
                "loc": [
                    "body",
                    "name1"
                ],
                "msg": "No permite nombre en minusculas",
                "type": "value_error"
            }
        ]
    }
def test_validacion_longitud_name1():
    """
        validar validar si supera la longitud maxima en el primer nombres
    """
    response = client.post(
        "/employee",
        json={
            "name1": "JUAN XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
            "name2": "GUILLERMO",
            "ape1": "DE LA CALLE MORAL",
            "ape2": "RR",
            "email": "string",
            "number_document": "1385129qr17",
            "date_entry": "2022-12-28",
            "fk_id_employment_country": 1,
            "fk_id_area": 1,
            "fk_id_document_type": 1
            }
    )
    # print(response.json())
    assert response.status_code == 422
    assert response.json() == {
            "detail": [
                {
                    "loc": [
                        "body",
                        "name1"
                    ],
                    "msg": "permite nombre un longitud maximo de 20 caracteres",
                    "type": "value_error"
                }
            ]
        }
# / test de name 1
# test de name 2
    """
        requerimiento: 
        Otros Nombres: Solo permite caracteres de la A a la Z,
        mayúscula, sin acentos ni Ñ, y el caracter espacio entre nombres.
        Es opcional y su longitud máxima será de 50 letras.
    """
def test_validacion_acentos_name2():
    """
        validar si tiene acentos en el primer appellido
    """
    response = client.post(
        "/employee",
        json={
                "name1": "JUAN",
                "name2": "GUILLERMÓ",
                "ape1": "DE LA CALLE MORAL",
                "ape2": "RR",
                "email": "string",
                "number_document": "1385129qr17",
                "date_entry": "2022-12-28",
                "fk_id_employment_country": 1,
                "fk_id_area": 1,
                "fk_id_document_type": 1
            },
    )
    # print(response.json())
    assert response.status_code == 422
    assert response.json() == {
        "detail": [
            {
                "loc": [
                    "body",
                    "name2"
                ],
                "msg": "No permite nombre caracteres especiales",
                "type": "value_error"
            }
        ]
    }
def test_validacion_minusculas_name2():
    """
        validar si tiene minusculas en el segundo nombre
    """
    response = client.post(
        "/employee",
        json={
                "name1": "JUAN",
                "name2": "GUILLERMo",
                "ape1": "DE LA CALLE MORAL",
                "ape2": "RR",
                "email": "string",
                "number_document": "1385129qr17",
                "date_entry": "2022-12-28",
                "fk_id_employment_country": 1,
                "fk_id_area": 1,
                "fk_id_document_type": 1
            },
    )
    # print(response.json())
    assert response.status_code == 422
    assert response.json() == {
        "detail": [
            {
                "loc": [
                    "body",
                    "name2"
                ],
                "msg": "No permite nombre en minusculas",
                "type": "value_error"
            }
        ]
    }
def test_validacion_longitud_name2():
    """
        validar si supera la longitud maxima en el segundo nombre
    """
    response = client.post(
        "/employee",
        json={
            "name1": "JUAN",
            "name2": "GUILLERMO XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
            "ape1": "DE LA CALLE MORAL",
            "ape2": "RR",
            "email": "string",
            "number_document": "1385129qr17",
            "date_entry": "2022-12-28",
            "fk_id_employment_country": 1,
            "fk_id_area": 1,
            "fk_id_document_type": 1
            }
    )
    # print(response.json())
    assert response.status_code == 422
    assert response.json() == {
            "detail": [
                {
                    "loc": [
                        "body",
                        "name2"
                    ],
                    "msg": "permite nombre un longitud maximo de 50 caracteres",
                    "type": "value_error"
                }
            ]
        }
# / test de name 2
# test de ape1
    """
        requerimento : 
        Primer Apellido: Solo permite caracteres de la A a la Z,
        mayúscula, sin acentos ni Ñ. 
        Es requerido y su longitud máxima será de 20 letras.
    """
def test_validacion_acentos_ape1():
    """
        validar si tiene acentos en el primer apellido
    """
    response = client.post(
        "/employee",
        json={
                "name1": "JUAN",
                "name2": "GUILLERMO",
                "ape1": "DE LA CALLÉ MORAL",
                "ape2": "RR",
                "email": "string",
                "number_document": "1385129qr17",
                "date_entry": "2022-12-28",
                "fk_id_employment_country": 1,
                "fk_id_area": 1,
                "fk_id_document_type": 1
            },
    )
    # print(response.json())
    assert response.status_code == 422
    assert response.json() == {
        "detail": [
            {
                "loc": [
                    "body",
                    "ape1"
                ],
                "msg": "No permite apellido caracteres especiales",
                "type": "value_error"
            }
        ]
    }
def test_validacion_minusculas_ape1():
    """
        validar si tiene minusculas en el primer apellido
    """
    response = client.post(
        "/employee",
        json={
                "name1": "JUAN",
                "name2": "GUILLERMO",
                "ape1": "DE LA CALLe MORAL",
                "ape2": "RR",
                "email": "string",
                "number_document": "1385129qr17",
                "date_entry": "2022-12-28",
                "fk_id_employment_country": 1,
                "fk_id_area": 1,
                "fk_id_document_type": 1
            },
    )
    # print(response.json())
    assert response.status_code == 422
    assert response.json() == {
        "detail": [
            {
                "loc": [
                    "body",
                    "ape1"
                ],
                "msg": "No permite apellido en minusculas",
                "type": "value_error"
            }
        ]
    }
def test_validacion_longitud_ape1():
    """
        validar si supera la longitud maxima en el primer apellido
    """
    response = client.post(
        "/employee",
        json={
            "name1": "JUAN",
            "name2": "GUILLERMO",
            "ape1": "DE LA CALLE MORAL XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
            "ape2": "RR",
            "email": "string",
            "number_document": "1385129qr17",
            "date_entry": "2022-12-28",
            "fk_id_employment_country": 1,
            "fk_id_area": 1,
            "fk_id_document_type": 1
            }
    )
    # print(response.json())
    assert response.status_code == 422
    assert response.json() == {
            "detail": [
                {
                    "loc": [
                        "body",
                        "ape1"
                    ],
                    "msg": "permite apellido un longitud maximo de 20 caracteres",
                    "type": "value_error"
                }
            ]
        }
# / test de ape1
# test de date_entry
    """
        requerimiento: 
        Fecha de ingreso: No podrá ser superior a la fecha actual,
        pero sí podrá ser hasta un mes menor,
        ya que es posible que el usuario no haya podido registrarlo el día en que ingresó.
    """
def test_validacion_max_entry():
    """
        valida la fecha no supere la fecha actual
    """
    response = client.post(
        "/employee",
        json={
                "name1": "JUAN",
                "name2": "GUILLERMO",
                "ape1": "DE LA CALLE MORAL",
                "ape2": "RR",
                "email": "string",
                "number_document": "1385129qr17",
                "date_entry": "2023-12-28",
                "fk_id_employment_country": 1,
                "fk_id_area": 1,
                "fk_id_document_type": 1
            },
    )
    # print(response.json())
    assert response.status_code == 422
    assert response.json() == {
        "detail": [
            {
                "loc": [
                    "body",
                    "date_entry"
                ],
                "msg": "No permite que la fecha de ingreso sea mayor a la fecha de hoy",
                "type": "value_error"
            }
        ]
    }
def test_validacion_min_entry():
    """
        valida la fecha de ingreso supere un mes anterior
    """
    response = client.post(
        "/employee",
        json={
                "name1": "JUAN",
                "name2": "GUILLERMO",
                "ape1": "DE LA CALLE MORAL",
                "ape2": "RR",
                "email": "string",
                "number_document": "1385129qr17",
                "date_entry": "2022-10-28",
                "fk_id_employment_country": 1,
                "fk_id_area": 1,
                "fk_id_document_type": 1
            },
    )
    # print(response.json())
    assert response.status_code == 422
    assert response.json() == {
        "detail": [
            {
                "loc": [
                    "body",
                    "date_entry"
                ],
                "msg": "fecha de ingreso  tiene un mes para que pueda registra el empleado",
                "type": "value_error"
            }
        ]
    }
# / test de date_entry