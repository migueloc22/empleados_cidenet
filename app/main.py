
from fastapi import FastAPI, Request

#routers
from routers.employee import employee
from routers.document_type import document_type
from routers.employment_country import employment_country
from routers.area import area
app = FastAPI()
app.include_router(area)
app.include_router(document_type)
app.include_router(employment_country)
app.include_router(employee)