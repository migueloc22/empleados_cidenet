from typing import Optional
from datetime import datetime, date,timedelta
from pydantic import BaseModel, validator
import re  # importando el modulo de regex de python
class Employee(BaseModel):
    id:Optional[int]
    name1:str
    name2: Optional[str]
    ape1: str
    ape2: str
    email:Optional[str]
    number_document:str
    status:Optional[str]
    date_time_register:Optional[datetime]
    date_entry:date
    fk_id_employment_country:Optional[int]
    fk_id_area:Optional[int]
    fk_id_document_type:Optional[int]
    @validator("name1")
    def validar_nombre(value):
        url = re.compile(r"^[A-Z]+$")
        print(url.search(value))
        if((value.isupper())==False):
            raise ValueError("No permite nombre en minusculas")
        if((url.search((value.replace(' ','')))) is None):
            raise ValueError("No permite nombre caracteres especiales")
        if len(value)>20:
            raise ValueError("permite nombre un longitud maximo de 20 caracteres")
        return value
    @validator("name2")
    def validar_nombre2(value):
        url = re.compile(r"^[A-Z]+$")
        print(url.search(value))
        if((value.isupper())==False):
            raise ValueError("No permite nombre en minusculas")
        if((url.search((value.replace(' ','')))) is None):
            raise ValueError("No permite nombre caracteres especiales")
        if len(value)>50:
            raise ValueError("permite nombre un longitud maximo de 50 caracteres")
        return value
    @validator("ape1")
    def validar_ape1(value):
        url = re.compile(r"^[A-Z]+$")
        print(url.search(value))
        if((value.isupper())==False):
            raise ValueError("No permite apellido en minusculas")
        if((url.search((value.replace(' ','')))) is None):
            raise ValueError("No permite apellido caracteres especiales")
        if len(value)>20:
            raise ValueError("permite apellido un longitud maximo de 20 caracteres")
        return value
    @validator("ape2")
    def validar_ape2(value):
        url = re.compile(r"^[A-Z]+$")
        print(url.search(value))
        if((value.isupper())==False):
            raise ValueError("No permite apellido en minusculas")
        if((url.search((value.replace(' ','')))) is None):
            raise ValueError("No permite apellido caracteres especiales")
        if len(value)>20:
            raise ValueError("permite apellido un longitud maximo de 20 caracteres")
        return value
    @validator("date_entry")
    def validar_date_entry(value):
        now_date =datetime.now()
        previous_date = datetime.now() - timedelta(days=30)
        if value>now_date.date():
            raise ValueError("No permite que la fecha de ingreso sea mayor a la fecha de hoy")
        if value<previous_date.date():
            raise ValueError("fecha de ingreso  tiene un mes para que pueda registra el empleado")
        return value
