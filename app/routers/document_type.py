from fastapi import APIRouter,Response,Depends,HTTPException
from config.db import conn
from models.document_type import document_types
document_type = APIRouter()
@document_type.get("/document_type",tags=['document_type'])
def get_document_type():
    return conn.execute(document_types.select()).fetchall()