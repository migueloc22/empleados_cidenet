from fastapi import APIRouter,Request,Response,Depends,HTTPException
from config.db import conn
from models.employment_country import employment_countrys
from models.area import areas
from models.document_type import document_types
from models.employee import employees
from schemas.employee import Employee
from sqlalchemy import func
from starlette.status import HTTP_204_NO_CONTENT
from sqlalchemy.sql.expression import select
employee = APIRouter()
@employee.get("/employee/page/{page}",tags=['employee'])
def get_employee(page:int):
    size = 10
    total_page = page*size
    query = select(
        employees.c.id,
        employees.c.name1,
        employees.c.name2,
        employees.c.ape1,
        employees.c.ape2,
        employees.c.email,
        employees.c.number_document,
        employees.c.status,
        func.date_format(employees.c.date_time_register, '%d/%m/%Y %H:%i:%s'),
        employees.c.date_entry,
        (document_types.c.name).label('document_type'),
        (employment_countrys.c.country).label('country'),
        (areas.c.area).label('area'),
        employees.c.fk_id_document_type,
        employees.c.fk_id_employment_country,
        employees.c.fk_id_area
    ).select_from(
        employees.join(document_types,employees.c.fk_id_document_type==document_types.c.id
        ).join(employment_countrys,employees.c.fk_id_employment_country==employment_countrys.c.id
        ).join(areas,employees.c.fk_id_area==areas.c.id)
    ).order_by(employees.c.id, employees.c.name1).limit(size).offset(total_page)
    print(query)
    return conn.execute(query).fetchall()
#{server}/employee/filter/0/?name1=''&name2=''&ape1=''&ape2=''&number_document=''&country=''
@employee.get("/employee/filter/{page}",tags=['employee'])
def get_employee_filter(page:int,name1:str='',name2:str='',ape1:str='',ape2:str='',number_document:str='',country:str=''):
    size = 10
    total_page = page*size
    where1 = (
                (employees.c.name1.like(f"{name1.upper()}")) |
                (employees.c.name2.like(f"%{name2.upper()}%")) |
                (employees.c.ape1.like(f"%{ape1.upper()}%")) |
                (employees.c.ape2.like(f"%{ape2.upper()}%")) |
                (employees.c.number_document.like(f"%{number_document.upper()}%")) |
                (employment_countrys.c.country.like(f"%{country.upper()}%")) 
            )
    query = select(
        employees.c.id,
        employees.c.name1,
        employees.c.name2,
        employees.c.ape1,
        employees.c.ape2,
        employees.c.email,
        employees.c.number_document,
        employees.c.status,
        func.date_format(employees.c.date_time_register, '%d/%m/%Y %H:%i:%s'),
        employees.c.date_entry,
        (document_types.c.name).label('document_type'),
        (employment_countrys.c.country).label('country'),
        (areas.c.area).label('area'),
        employees.c.fk_id_document_type,
        employees.c.fk_id_employment_country,
        employees.c.fk_id_area
    ).select_from(
        employees.join(document_types,employees.c.fk_id_document_type==document_types.c.id
        ).join(employment_countrys,employees.c.fk_id_employment_country==employment_countrys.c.id
        ).join(areas,employees.c.fk_id_area==areas.c.id)
    ).where(
        where1
    ).limit(size).offset(total_page)
    print(query)
    return conn.execute(query).fetchall()
@employee.post("/employee",tags=['employee'])
def add_employee(employee:Employee):
    employee.email = f"{employee.name1.replace(' ','').lower()}.{employee.ape1.replace(' ','').lower()}@cidenet.com.co"
    if validar_email(employee.email)==False:
        employee.email = f"{employee.name1.replace(' ','').lower()}.{employee.ape1.replace(' ','').lower()}.{employee.number_document.replace(' ','')}@cidenet.com.co"
        if (validar_email(employee.email))==False:
           raise HTTPException(status_code=400, detail="El correo se encuentra ya registrado en el sistema")
    new_employee = {
        "name1":employee.name1,
        "name2":employee.name2,
        "ape1":employee.ape1,
        "ape2":employee.ape2,
        "email":employee.email,
        "number_document":employee.number_document,
        "date_entry":employee.date_entry,
        "fk_id_employment_country":employee.fk_id_employment_country,
        "fk_id_area":employee.fk_id_area,
        "fk_id_document_type":employee.fk_id_document_type,
    }
    query = employees.insert().values(new_employee)
    conn.execute(query)
    return employee
@employee.put("/employee/{id}",tags=['employee'])
def update_employee(id:int,employee:Employee):
    employee.email = f"{employee.name1.replace(' ','').lower()}.{employee.ape1.replace(' ','').lower()}@cidenet.com.co"
    if validar_email(employee.email)==False:
        employee.email = f"{employee.name1.replace(' ','').lower()}.{employee.ape1.replace(' ','').lower()}.{employee.number_document.replace(' ','')}@cidenet.com.co"
        if (validar_email(employee.email))==False:
           raise HTTPException(status_code=400, detail="El correo se encuentra ya registrado en el sistema")
    new_employee = {
        "name1":employee.name1,
        "name2":employee.name2,
        "ape1":employee.ape1,
        "ape2":employee.ape2,
        "email":employee.email,
        "number_document":employee.number_document,
        "date_entry":employee.date_entry,
        "fk_id_employment_country":employee.fk_id_employment_country,
        "fk_id_area":employee.fk_id_area,
        "fk_id_document_type":employee.fk_id_document_type,
    }
    query = employees.update().values(new_employee).where(employees.c.id==id)
    conn.execute(query)
    return employee
@employee.delete("/employee/{id}",tags=['employee'])
def delete_turns(id:str):
    query= employees.delete().where(employees.c.id==id)
    conn.execute(query)
    return Response(status_code=HTTP_204_NO_CONTENT)
def validar_email(email):
    """
      funcion que validar si existe el empleado ya  exsite registrado la base de datos 

    Args:
        email (String): parametro para filtrar en la base de datos con campo email de la tabla employees

    Returns:
        boolean: retorna falso si existe y true no existe el email en la base de datos
    """
    val= True
    query = employees.select().where(employees.c.email == email)
    result = conn.execute(query).first()
    if result is not None:
        val= False
    return val
def validar_email_idUser(email,id):
    """
      funcion que validar si existe empleado cambio imei

    Args:
        email (String): parametro para filtrar en la base de datos con campo email de la tabla employees

    Returns:
        boolean: valiada si se cambio el email
    """
    val= False
    query = employees.select().where(employees.c.email == email)
    result = conn.execute(query).first()
    if result is not None:
        val= True
    return val