from fastapi import APIRouter,Response,Depends,HTTPException
from config.db import conn
from models.area import areas
area = APIRouter()
@area.get("/area",tags=['area'])
def get_area():
    return conn.execute(areas.select()).fetchall()