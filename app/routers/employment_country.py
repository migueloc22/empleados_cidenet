from fastapi import APIRouter,Response,Depends,HTTPException
from config.db import conn
from models.employment_country import employment_countrys
employment_country = APIRouter()
@employment_country.get("/employment_country",tags=['employment_country'])
def get_employment_country():
    return conn.execute(employment_countrys.select()).fetchall()