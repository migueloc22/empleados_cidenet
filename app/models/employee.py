from sqlalchemy import Table,Column,ForeignKey
from sqlalchemy.sql.expression import true
from sqlalchemy.sql.sqltypes import Date,DateTime, Integer, String
from sqlalchemy.sql import func
from config.db import meta,engine
employees = Table("employees",meta,
    Column("id",Integer,primary_key=True),
    Column("name1",String(20)),
    Column("name2",String(50),nullable=true),
    Column("ape1",String(20)),
    Column("ape2",String(20)),
    Column("email",String(300),unique=True,nullable=true),
    Column("number_document",String(12)),
    Column("status",String(20),server_default='ACTIVO'),
    Column("date_time_register",DateTime(timezone=True), server_default=func.now()),
    Column("date_entry",Date()),
    Column('fk_id_employment_country',ForeignKey('employment_country.id')),
    Column('fk_id_area',ForeignKey('area.id')),
    Column('fk_id_document_type',ForeignKey('document_type.id')),
    )
meta.create_all(engine)