from sqlalchemy import Table,Column
from sqlalchemy.sql.operators import ColumnOperators
from sqlalchemy.sql.sqltypes import Integer, String
from config.db import meta,engine
areas = Table("area",meta,
    Column("id",Integer,primary_key=True),
    Column("area",String(70)),
)
meta.create_all(engine)