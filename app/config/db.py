from sqlalchemy import create_engine, MetaData
from sqlalchemy.pool import StaticPool
# sqlite
# engine = create_engine('sqlite+pysqlite3:///test.db',connect_args={'check_same_thread': False},poolclass=StaticPool)
# mysql mysql+pymysql://<username>:<password>@<host>/<dbname>[?<options>]
engine = create_engine("mysql+pymysql://testuser:123pass@localhost:3306/cidenet_employee",encoding="utf-8",echo=True)
meta = MetaData()
conn = engine.connect()